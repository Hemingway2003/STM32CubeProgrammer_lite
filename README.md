## How to use

Download the released zip file and unzip it, add the `STM32CubeProgrammer_lite\bin` path to system environment

To Download the `hex` or `bin` file, run in `cmd`:

```
STM32_Programmer_CLI.exe -c port=SWD freq=4000 mode=NORMAL -d build\stm32_test.hex 0x08000000 -v -s 0x08000000
```

- This will start at `0x08000000`

or 

```
STM32_Programmer_CLI.exe -c port=SWD freq=4000 mode=NORMAL -d build\stm32_test.hex 0x08000000 -v -rst
```

- This will reset the whole device

I still recommend run `date /t & time /t` after the download command strongly, this will show your download time.

## 如何使用

下载发行版的zip文件并解压，把`STM32CubeProgrammer_lite\bin`路径添加到系统环境变量中

要下载`hex`或者`bin`文件，在`cmd`中运行：

```
STM32_Programmer_CLI.exe -c port=SWD freq=4000 mode=NORMAL -d build\stm32_test.hex 0x08000000 -v -s 0x08000000
```

- 这个会让设备从`0x08000000`地址运行

或者

```
STM32_Programmer_CLI.exe -c port=SWD freq=4000 mode=NORMAL -d build\stm32_test.hex 0x08000000 -v -rst
```

- 这个会重启整个设备

我十分建议在下载命令后运行`date /t & time /t`，这个会显示你下载的时间。